
void setup() {  
  Serial.begin(9600);   // Inicializa la comunicación serial por defecto
  delay(100);
  Serial.print("Proporciona un entero:");
}


void loop() {
    
    if(Serial.available()){
      String cadena=Serial.readStringUntil('\n');
      Serial.println(cadena);
      int n=cadena.toInt();
      for(int i=1;i<=10;i++)
          Serial.printf("%3d * %3d=%3d\n",n,i,n*i);
      Serial.print("Proporciona un entero:");
    }
}
